(() => {
    const id = `living-bank-helper`;
    const title = 'Living Bank Helper';
    const desc = 'Displays which items are missing from your bank.';
    const imgSrc = getItemMedia(CONSTANTS.item.Bank_Slot_Token);

    const emptyObj = { media: 'assets/media/main/question.svg', name: '???' };

    const el = (i) => {
        const empty = !SEMIUtils.isItemFound(i);
        const { media, name } = empty ? emptyObj : items[i];

        const e = $(
            `<img id="${id}-img-${i}" class="skill-icon-sm js-tooltip-enable border border-white border-2" src="${getItemMedia(
                i
            )}" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="${name}">`
        );

        if (empty) {
            e.fadeTo(500, 0.25);
        }

        return e;
    };

    const getMissingItems = () => {
        const onlyShowTowardsCompletion = SEMI.getItem(`${id}-towards-completion`);

        // Gets the items you're missing and subtracts them out
        const bankItemIDs = bank.map((x) => x.id);
        const allItemIDs = items.map((x) => x.id);
        const missingIDs = allItemIDs.filter((itemID) => {
            if (onlyShowTowardsCompletion) {
                return !bankItemIDs.includes(itemID) && !items[itemID].ignoreCompletion;
            } else {
                return !bankItemIDs.includes(itemID);
            }
        });

        return missingIDs;
    };

    const getNeededBankSlots = () => {
        const numberOfItemsLeft = getMissingItems().length;

        const openSlotsInBank = getMaxBankSpace() - bank.length;

        return numberOfItemsLeft - openSlotsInBank;
    };

    const autoShow = () => {
        $(`#${id}-container`).html('');

        const missingIDs = getMissingItems();

        for (let i = 0; i < missingIDs.length; i++) {
            $(`#${id}-container`).append(el(missingIDs[i]));
        }

        $(`#${id}-bank-slots`).text(getNeededBankSlots());

        $(`#modal-${id}`).modal('show');
    };

    const onlyShowTowardsCompletionToggle = () => {
        const onlyShowTowardsCompletion = SEMI.getItem(`${id}-towards-completion`);

        if (onlyShowTowardsCompletion) {
            $(`#${id}-towards-completion`).addClass('btn-danger');
            $(`#${id}-towards-completion`).removeClass('btn-success');
        } else {
            $(`#${id}-towards-completion`).addClass('btn-success');
            $(`#${id}-towards-completion`).removeClass('btn-danger');
        }

        SEMI.setItem(`${id}-towards-completion`, !onlyShowTowardsCompletion);

        SEMIUtils.customNotify(imgSrc, 'Re-open the window to see the updated listing');
    };

    const injectGUI = () => {
        const x = $('#modal-item-log').clone().first();
        x.attr('id', `modal-${id}`);
        $('#modal-item-log').parent().append(x);
        const y = $(`#modal-${id}`).children().children().children().children('.font-size-sm');
        y.children().children().attr('id', `${id}-container`);

        $(`#modal-${id} .block-title`).text(`${title} Menu`);

        const onlyShowTowardsCompletion = SEMI.getItem(`${id}-towards-completion`);
        const controlSection = $(`
        <div class="col-12 bg-gray-400">
            <div class="flex justify-center">
                <div class="py-2 px-2">
                    <div class="row row-deck gutters-tiny">
                        <div class="col-2 col-md-2"></div>
                        <div class="col-6 col-md-4">
                            <button class="btn btn-md btn-${
                                onlyShowTowardsCompletion ? 'success' : 'danger'
                            } SEMI-modal-btn" id="${id}-towards-completion">
                                Only Show Items Towards Completion
                            </button>
                        </div>
                        <div class="col-4 col-md-4 my-1">
                            <b>Needed Bank Slots:</b>&nbsp;<div id="${id}-bank-slots"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`);

        y.before(controlSection);

        const onlyShowTowardsCompletionButton = $(`button#${id}-towards-completion`);
        onlyShowTowardsCompletionButton.on('click', () => onlyShowTowardsCompletionToggle());

        setTimeout(() => {
            $(`#${id}-menu-button`).on('click', autoShow);
            $(`#${id}-menu-button`).attr('href', null);
        }, 1000);
    };

    SEMI.add(id + '-menu', {
        ms: 0,
        title,
        desc,
        imgSrc,
        injectGUI,
        pluginType: SEMI.PLUGIN_TYPE.TWEAK,
    });
})();

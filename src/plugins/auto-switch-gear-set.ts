(() => {
    const id = 'auto-switch-gear-set';
    const title = 'Auto Switch Gear Set';
    const desc =
        'Auto Switch Gear Set will switch your current gear set to match the combat style of the enemy you are currently facing. If you do not have valid sets equipped to swap to, exits combat. This does not care if you have no armor, so do have full sets on all slots.';
    const imgSrc = getItemMedia(CONSTANTS.item.Slayer_Helmet_Master);

    const isInCombat = () => combatManager.isInCombat;

    const enemyStyle = () => combatManager.enemy.attackType;

    const getSetAttackTypes = () => {
        return player.equipmentSets.map((x) => x.slots['Weapon']?.item.attackType);
    };

    const getWantedSet = () => {
        const currentSets = getSetAttackTypes();

        if (enemyStyle() === 'magic') {
            return currentSets.indexOf('ranged');
        }
        if (enemyStyle() === 'ranged') {
            return currentSets.indexOf('melee');
        }
        if (enemyStyle() === 'melee') {
            return currentSets.indexOf('magic');
        }
    };

    const autoGearSwap = () => {
        // Exit if not in combat
        if (!isInCombat()) {
            return;
        }

        // Find the set we want
        const wantedSetIndex = getWantedSet();

        // If we don't have a valid set, exit
        if (wantedSetIndex < 0) {
            SEMIUtils.customNotify(imgSrc, 'WARNING: You do not have a valid equipment set. Running from combat.');
            SEMIUtils.stopSkill('Hitpoints');
        }

        // If we have the right set, stay put
        if (wantedSetIndex === player.selectedEquipmentSet) {
            return;
        }

        // Gets the prayers for the set we want, saves the current, then toggles them over
        const wantedSetPrayers = SEMI.getValue(id, `prayers-${wantedSetIndex}`);
        SEMI.setValue(id, `prayers-${player.selectedEquipmentSet}`, player.activePrayers);

        player.activePrayers.forEach((x) => player.togglePrayer(x));
        wantedSetPrayers.forEach((x) => player.togglePrayer(x));

        // Gets the current spells for the set we want, then toggles those on
        const wantedSetSpells = SEMI.getValue(id, `spells-${wantedSetIndex}`);
        SEMI.setValue(id, `spells-${player.selectedEquipmentSet}`, player.spellSelection);

        player.changeEquipmentSet(wantedSetIndex);
    };

    SEMI.add(id, {
        ms: 1000,
        onLoop: autoGearSwap,
        pluginType: SEMI.PLUGIN_TYPE.AUTO_COMBAT,
        title,
        imgSrc,
        desc,
    });
})();

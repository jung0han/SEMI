var autoSellShow = (() => {
    const id = `auto-sell`;
    const title = 'AutoSell';
    const desc = 'AutoSell is a script for selling items you no longer want.';
    const imgSrc = 'assets/media/main/gp.svg';

    const faceOpacity = 0.25;
    const hiddenOpacity = 0;

    let isItemEnabledToSell = {};
    let isItemMatchFilter = {};

    const fadeAll = () => {
        for (let i = 0; i < SEMIUtils.itemsLength(); i++) {
            const x = $(`#${id}-img-${i}`);
            if (x.length === 0) {
                continue;
            }
            const shouldBeFaded = !isItemEnabledToSell[i];
            const shouldBeHidden = Object.keys(isItemMatchFilter).length === 0 ? false : !isItemMatchFilter[i];
            const currentState = typeof x.css('opacity') === 'undefined' ? '1' : x.css('opacity');
            const isFaded = currentState === faceOpacity + '';
            const isHidden = currentState === hiddenOpacity + '';

            if (!shouldBeHidden) {
                x.css('pointer-events', '');
            }

            if (shouldBeHidden) {
                if (!isHidden) {
                    x.css('pointer-events', 'none');
                    x.fadeTo(500, hiddenOpacity);
                }
            } else if (shouldBeFaded) {
                if (!isFaded) {
                    x.fadeTo(500, faceOpacity);
                }
            } else if (isFaded || isHidden) {
                x.fadeTo(500, 1);
            }
        }
    };

    /** @param {number} i */
    const toggleAutoEnabled = (i) => {
        isItemEnabledToSell[i] = !isItemEnabledToSell[i];
        autoSell();
        fadeAll();
    };

    const el = (i) => {
        const empty = !SEMIUtils.isItemFound(i);
        const name = items[i].name;

        let media;
        if (empty) {
            media = 'assets/media/main/question.svg';
        } else {
            media = getItemMedia(i);
        }

        const e = $(
            `<img id="${id}-img-${i}" class="skill-icon-sm js-tooltip-enable border border-white border-2" src="${media}" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="${name}">`
        );

        e.on('click', () => toggleAutoEnabled(i));

        return e;
    };

    const autoSell = () => {
        const allButOneIsEnabled = SEMI.getItem(`${id}-sell-one`);

        for (let i = bank.length - 1; i >= 0; i--) {
            const itemID = bank[i].id;
            if (isItemEnabledToSell[itemID] && SEMI.isEnabled(id)) {
                let qty;
                if (allButOneIsEnabled) {
                    qty = bank[i].qty - 1;
                } else {
                    qty = bank[i].qty;
                }

                // If All But One reduces the count of available openings to 0, skip to next
                if (qty === 0) {
                    continue;
                }

                const gpBefore = gp;
                SEMIUtils.sellItemWithoutConfirmation(itemID, qty);
                SEMIUtils.customNotify(
                    items[itemID].media,
                    `Selling ${numberWithCommas(qty)} of ${items[itemID].name} for ${numberWithCommas(
                        gp - gpBefore
                    )} GP`,
                    { lowPriority: true }
                );
            }
        }
    };

    const setupContainer = () => {
        $(`#${id}-container`).html('');
        for (let i = 0; i < SEMIUtils.itemsLength(); i++) {
            $(`#${id}-container`).append(el(i));
        }

        const loadedAutoEnabled = SEMI.getItem(`${id}-config`);
        if (loadedAutoEnabled !== null) {
            // Migrate old format
            if (Array.isArray(loadedAutoEnabled)) {
                for (let i = 0; i < loadedAutoEnabled.length; i++) {
                    if (loadedAutoEnabled[i]) {
                        isItemEnabledToSell[i] = true;
                    }
                }
            } else {
                isItemEnabledToSell = loadedAutoEnabled;
            }
        }

        fadeAll();
    };

    const onDisable = () => {
        $(`#${id}-status`).addClass('btn-danger');
        $(`#${id}-status`).removeClass('btn-success');
    };

    const onEnable = () => {
        $(`#${id}-status`).addClass('btn-success');
        $(`#${id}-status`).removeClass('btn-danger');

        autoSell();
    };

    const autoShow = () => {
        $(`#modal-${id}`).modal('show');
    };

    const allButOneToggle = () => {
        const allButOneIsEnabled = SEMI.getItem(`${id}-sell-one`);

        if (allButOneIsEnabled) {
            $(`#${id}-sell-one`).addClass('btn-danger');
            $(`#${id}-sell-one`).removeClass('btn-success');
        } else {
            $(`#${id}-sell-one`).addClass('btn-success');
            $(`#${id}-sell-one`).removeClass('btn-danger');
        }

        SEMI.setItem(`${id}-sell-one`, !allButOneIsEnabled);
    };

    const injectGUI = () => {
        const x = $('#modal-item-log').clone().first();
        x.attr('id', `modal-${id}`);
        $('#modal-item-log').parent().append(x);
        const y = $(`#modal-${id}`).children().children().children().children('.font-size-sm');
        y.children().children().attr('id', `${id}-container`);

        const allButOne = SEMI.getItem(`${id}-sell-one`);
        const controlSection = $(`
        <div class="col-12 bg-gray-400">
            <div class="block block-rounded py-2 px-2">
                <div class="row row-deck gutters-tiny">
                <div class="col-2 col-md-2">
                    <button class="btn btn-md btn-danger SEMI-modal-btn" id="${id}-status">Disabled</button>
                </div>
                <div class="col-2 col-md-2">
                    <button class="btn btn-md btn-${
                        allButOne ? 'success' : 'danger'
                    } SEMI-modal-btn" id="${id}-sell-one">All But One</button>
                </div>
                <div class="col-8 col-md-8">
                    <input class="form-control" type="text" id="${id}-filter" placeholder="Search for items ... (text | regex)">
                </div>
                </div>
            </div>
        </div>`);

        y.before(controlSection);

        const enableAutoButton = $(`button#${id}-status`);
        const enableAllButOneButton = $(`button#${id}-sell-one`);
        const searchInput = $(`input#${id}-filter`);

        enableAutoButton.on('click', () => SEMI.toggle(`${id}`));
        enableAllButOneButton.on('click', () => allButOneToggle());

        searchInput.on('change', (event) => {
            const value = event.currentTarget.value;
            if (!value || value.length === 0) {
                isItemMatchFilter = {};
                fadeAll();
                return;
            }

            const _value = RegExp(value.replace(/^\/|\/$/g, ''), 'i');
            isItemMatchFilter = {};
            for (let i = 0; i < SEMIUtils.itemsLength(); i++) {
                const { name } = items[i];
                if (name.match(_value)) {
                    isItemMatchFilter[i] = true;
                }
            }
            fadeAll();
        });

        $(`#modal-${id} .block-title`).text(`${title} Menu`);

        $(`#modal-${id}`).on('hidden.bs.modal', () => {
            SEMI.setItem(`${id}-config`, isItemEnabledToSell);
        });
        setupContainer();
        setTimeout(() => {
            $(`#${id}-menu-button`).on('click', autoShow);
            $(`#${id}-menu-button`).attr('href', null);
        }, 1000);
    };

    SEMI.add(id, {
        ms: 50,
        onEnable,
        onDisable,
        onLoop: autoSell,
        title,
        desc,
    });

    SEMI.add(id + '-menu', {
        title,
        desc,
        imgSrc,
        injectGUI,
    });
})();

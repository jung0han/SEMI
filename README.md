# Scripting Engine for Melvor Idle

### **SEMI** is an **UNOFFICIAL** Firefox and Chrome web extension for Melvor Idle.

**As with any scripts or game mods, please back up your data before making any changes!**

[![Mozilla Firefox](https://img.shields.io/amo/v/scripting-engine-melvor-idle?label=Get%20SEMI%20for%20Firefox&logo=firefox)](https://addons.mozilla.org/en-US/firefox/addon/scripting-engine-melvor-idle/)
[![Google Chrome](https://img.shields.io/chrome-web-store/v/mnjfmmpkdmgfpabgbeoclagnclmpmjgm?label=Get%20SEMI%20for%20Chrome&logo=Google%20Chrome)](https://chrome.google.com/webstore/detail/scripting-engine-for-melv/mnjfmmpkdmgfpabgbeoclagnclmpmjgm?authuser=0&hl=en)

New: you are able to download pre-built releases of SEMI in a .zip file, handy for manual installation in your browser or installation into Steam. Go to the [SEMI releases page](https://gitlab.com/aldousWatts/SEMI/-/releases) and click on the 'build' link under 'Others' to download your zip, then see [the SEMI Installation Instructions Wiki Page](https://gitlab.com/aldousWatts/SEMI/-/wikis/Installation-Instructions).

The maintainers **HIGHLY** recommend using the M3 Mod Launcher for Steam users. M3 makes it trivial to download updates to SEMI and plenty of other extensions. [Link to M3 here](https://github.com/CherryMace/melvor-mod-manager)

## Contributors

Huge thanks to DanielRX, Visua, Breindahl, AuroraKy, Zeldo, Parataku, Alex Mauney, Shamus Taylor, TheAlpacalypse, and many more for helping with the project!

## What can SEMI do?

This add-on/extension helps you automate certain aspects of the game, and adds certain useful features, combining many scripts into one. Toggle each one on and off individually from inside the game. Hovering over the SEMI sidebar buttons will give tooltips including hints, explanations, and tips for most scripts.

See the [SEMI Features Wiki Page](https://gitlab.com/aldousWatts/SEMI/-/wikis/SEMI-Features) for a detailed list of scripts and features!

## Bugs & Requests

Notice a bug? Have an idea for something SEMI can do?

You can post [here on the issues page of the SEMI repository](https://gitlab.com/aldousWatts/SEMI/-/issues), chat about it in the Melvor discord in the #scripting-and-extensions channel, and/or talk to Aldous Watts.

## Goal of the Software

This software was made to unify many Melvor automation and QOL scripts, including my own, into one easy-to-use platform with a UI that mirrors the game, without worrying about compatibility or maintaining individual userscripts.

## Thanks Again

This project has been maintained primarily by the community. Thanks to all the helpful developers out there! And thanks to all the users, it's all for you!
